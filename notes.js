const fs = require('fs')
const chalk = require('chalk')

const addNote = (title,body) =>{
    const notes = loadNotes()
    const duplicateNote = notes.find((note)=> note.title === title)

    if(!duplicateNote){
        notes.push({
            title: title,
            body: body
        })
    
        saveNotes(notes)
        console.log(chalk.bgGreen('New note added!'))
    }else{
        console.log(chalk.bgRed('Note title taken!'))
    }
}

const removeNote = (title) =>{
    const notes = loadNotes()
    const savedNotes = notes.filter((note) => note.title !== title)

    if(notes.length !== savedNotes.length){
        console.log(chalk.bgGreen('Note removed!'))
        saveNotes(savedNotes)
    }else{
        console.log(chalk.bgRed('No note found!'))
    }
}


const saveNotes = (notes) =>{
    const dataJSON = JSON.stringify(notes)
    fs.writeFileSync('notes.json', dataJSON)
}

const loadNotes = () =>{
    try{
        const dataBuffer = fs.readFileSync('notes.json')
        const dataJSON = dataBuffer.toString()
        return JSON.parse(dataJSON)
    }catch(e){
        return []
    }
}

const listNotes = () => {
    console.log(chalk.bgBlue('Your Notes:'))

    const notes = loadNotes()
    notes.forEach(note => {
        console.log(note.title)
    });
}

const readNote = (title) => {
    const notes = loadNotes()
    const foundNote = notes.find((note)=> note.title === title)

    if(foundNote){
        console.log(chalk.bgBlue(foundNote.title))
        console.log(foundNote.body)
    }else{
        console.log(chalk.bgRed('No note found!'))
    }
}

module.exports = {
    addNote: addNote,
    removeNote: removeNote,
    listNotes: listNotes,
    readNote: readNote
}
