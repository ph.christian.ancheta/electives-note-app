# Notes App

## Setup

Run `npm install` on the root directory

## Run the Application

### List Notes

Run `node app.js list`

### Read a Note

Run `node app.js read --title="<an existing title from list>"`

### Add Note

Run `node app.js add --title="<your title here>" --body="<your body here>"`

### Remove Note

Run `node app.js remove --title="<an existing title from list>"`
